# EKS-Managed-Node-Script
This repository consist of a Bash Script to automate the scaling up and down of Amazon Elastic Kubernetes Service cluster managed node group. The Gitlab CICD Pipeline is expected to run the script(managed_node.sh) at a schedule time of 1 hours every day and execute the script loop condition between 9pm and 9am cst to scale the nodes down to 1 and scale back at regular time.


## Scaling the managed nodes up at business hours 
![GitHub actions variable](./img/managed_node_up.jpg)

## Scaling the managed nodes down at after hours 
![GitHub actions variable](./img/github_actions.jpg)

